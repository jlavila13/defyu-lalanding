$(document).ready(function(){
   //$('.slide-holder').flexslider();

    //modales
    $('.defyu-play').click(function(){
       $('body').addClass('lock-body');
       $('.video').removeClass('hide-modal');
       $('.video').addClass('show-modal');
    });
    $('.legal').click(function(){
       $('body').addClass('lock-body');
       $('.terms').addClass('show-modal');
       $('.terms').removeClass('hide-modal');
    });
    $('.login-but').click(function(){
       $('body').addClass('lock-body');
       $('.login-form').addClass('show-modal');
       $('.login-form').removeClass('hide-modal');
    });
    $('.fa-close').click(function(){
        $(this).parent().addClass('hide-modal');
        $('body').removeClass('lock-body');
    });

    //counter
    $('.counter').each(function() {
      var $this = $(this),
          countTo = $this.attr('data-count');

      $({ countNum: $this.text()}).animate({
        countNum: countTo
      },

      {

        duration: 8000,
        easing:'linear',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
          //alert('finished');
        }

      });



    });

    //tabs

    $('.tabs, .bullet-selector').each(function(){
      // For each set of tabs, we want to keep track of
      // which tab is active and its associated content
      var $active, $content, $links = $(this).find('a');

      // If the location.hash matches one of the links, use that as the active tab.
      // If no match is found, use the first link as the initial active tab.
      $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
      $active.addClass('active');

      $content = $($active[0].hash);

      // Hide the remaining content
      $links.not($active).each(function () {
        $(this.hash).hide();
      });

      // Bind the click event handler
      $(this).on('click', 'a', function(e){
        // Make the old tab inactive.
        $active.removeClass('active');
        $content.fadeOut();

        // Update the variables with the new link and content
        $active = $(this);
        $content = $(this.hash);

        // Make the tab active.
        $active.addClass('active');
        $content.fadeIn();
        $content.addClass('tab-active');

        // Prevent the anchor's default click action
        e.preventDefault();
      });
    });
    $('.slide-holder ul li:nth-child(1)').show().addClass('tab-active');



    if($('.tab-holder li').hasClass('tab-active')){
      console.log('tab activa pal beta');
    }



    //scrollTo
    /*$('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });*/

});


